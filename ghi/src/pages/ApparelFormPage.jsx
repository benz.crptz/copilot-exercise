import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import NavBar from '../components/NavBar';


const ApparelFormPage = () => {
    const [apparel, setApparel] = useState({
        item: '',
        description: '',
        size: '',
        price: '',
    });
    const { apparelId } = useParams();
    const navigate = useNavigate();

    const fetchApparel = async () => {
        const url = `${import.meta.env.VITE_API_HOST}/apparels/${apparelId}`;
        const res = await fetch(url, {
            credentials: 'include',
        });
        if (res.ok) {
            const newApparel = await res.json();
            setApparel(newApparel);
        } else {
            console.error('Failed to fetch apparel');
        }
    };

    useEffect(() => {
        if (apparelId) {
            fetchApparel();
        }
    }, [apparelId]);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setApparel({
            ...apparel,
            [name]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `${import.meta.env.VITE_API_HOST}/apparels${apparelId ? `/${apparelId}` : ''}`;
        const method = apparelId ? 'PATCH' : 'POST';
        const result = await fetch(url, {
            method,
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(apparel),
        });
        if (result.ok) {
            navigate('/apparels');
        } else {
            console.error('Failed to save apparel');
        }
    };

    return (
        <>
            <NavBar />
            <div id="page-container" className="form-container">
                <form onSubmit={handleSubmit}>
                    <label>Name:</label>
                    <input
                        type="text"
                        value={apparel.item}
                        name="item"
                        onChange={handleInputChange}
                    />
                    <br />
                    <label>Description:</label>
                    <input
                        type="text"
                        value={apparel.description}
                        name="description"
                        onChange={handleInputChange}
                    />
                    <br />
                    <label>Size:</label>
                    <input
                        type="text"
                        value={apparel.size}
                        name="size"
                        onChange={handleInputChange}
                    />
                    <br />
                    <label>Price:</label>
                    <input
                        type="number"
                        value={apparel.price}
                        name="price"
                        onChange={handleInputChange}
                    />
                    <br />
                    <button type="submit">
                        {apparelId ? 'Update Apparel' : 'Create New Apparel'}
                    </button>
                </form>
            </div>
        </>
    );
};

export default ApparelFormPage;
