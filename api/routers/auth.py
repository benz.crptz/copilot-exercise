from fastapi import Depends, Request, Response, HTTPException, status, APIRouter
from pydantic import BaseModel

from queries.employee import EmployeeQueries, EmployeeIn, EmployeeOut
from auth_service import auth_service, AuthResponse


class EmployeeSignIn(BaseModel):
    email: str
    password: str

router = APIRouter()

# signup (also signs in)
@router.post("/api/employees")
async def signup(
    new_employee: EmployeeIn,
    request: Request,
    response: Response,
    queries: EmployeeQueries = Depends(),
) -> AuthResponse:
    hashed_password = auth_service.hash_password(new_employee.password)
    user = queries.create_employee(new_employee, hashed_password)
    user_out = EmployeeOut(**user.dict())
    token = auth_service.get_jwt(user_out)
    samesite, secure = auth_service.get_cookie_settings(request)
    response.set_cookie(
        key=auth_service.cookie_name,
        value=token,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return AuthResponse(token=token, type="Bearer", user=user_out)

# signin
@router.post("/token")
async def signin(
    user_data: EmployeeSignIn,
    request: Request,
    response: Response,
    queries: EmployeeQueries = Depends(),
) -> AuthResponse:
    try:
        user = queries.get_by_email(user_data.email)
        auth_service.verify_password(user_data.password, user.hashed_password)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    user_out = EmployeeOut(**user.dict())
    token = auth_service.get_jwt(user_out)
    samesite, secure = auth_service.get_cookie_settings(request)
    response.set_cookie(
        key=auth_service.cookie_name,
        value=token,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return AuthResponse(token=token, type="Bearer", user=user_out)

# refresh
@router.get("/token")
async def get_token(
    request: Request,
    user: dict = Depends(auth_service.try_get_current_user_data)
) -> AuthResponse:
    if user and auth_service.cookie_name in request.cookies:
        return {
            "token": request.cookies[auth_service.cookie_name],
            "type": "Bearer",
            "user": user,
        }

# signout
@router.delete("/token")
async def signout(
    request: Request,
    response: Response,
) -> dict:
    samesite, secure = auth_service.get_cookie_settings(request)
    response.delete_cookie(
        key=auth_service.cookie_name,
        httponly=True,
        samesite=samesite,
        secure=secure,
    )
    return True
