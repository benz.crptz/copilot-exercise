from fastapi import APIRouter, HTTPException, Depends
from queries.shop_apparel import ShopApparelQueries, ShopApparelOut, ShopApparelIn


router = APIRouter()

@router.post("/shop_apparels", response_model=ShopApparelOut)
async def create_shop_apparel_endpoint(
    shop_apparel: ShopApparelIn,
    queries: ShopApparelQueries = Depends(),
):
    new_shop_apparel = queries.create_shop_apparel(shop_apparel)
    if new_shop_apparel is None:
        raise HTTPException(status_code=400, detail="Shop apparel could not be created")
    return new_shop_apparel

@router.get("/shop_apparels", response_model=list[ShopApparelOut])
async def read_shop_apparels_endpoint(
    queries: ShopApparelQueries = Depends(),
):
    shop_apparels = queries.get_all()
    return shop_apparels

@router.patch("/shop_apparels", response_model=ShopApparelOut)
async def update_shop_apparel_endpoint(
    shop_apparel: ShopApparelIn,
    queries: ShopApparelQueries = Depends(),
):
    updated_shop_apparel = queries.update_shop_apparel(shop_apparel)
    if updated_shop_apparel is None:
        raise HTTPException(status_code=404, detail="Shop apparel not found")
    return updated_shop_apparel

@router.delete("/shop_apparels/{apparel_id}/{shop_id}")
async def delete_shop_apparel_endpoint(
    apparel_id: int,
    shop_id: int,
    queries: ShopApparelQueries = Depends(),
):
    deleted = queries.delete_shop_apparel(apparel_id, shop_id)
    if not deleted:
        raise HTTPException(status_code=404, detail="Shop apparel not found")
    return {"detail": "Shop apparel deleted"}
