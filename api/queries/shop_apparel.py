import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class ShopApparelIn(BaseModel):
    shop_id: int
    apparel_id: int
    quantity: int

class ShopApparelOut(ShopApparelIn):
    id: int

def sanitize_shop_apparel(shop_apparels):
    return [
        ShopApparelOut(
            id=shop_apparel[0],
            shop_id=shop_apparel[1],
            apparel_id=shop_apparel[2],
            quantity=shop_apparel[3],
        )
        for shop_apparel in shop_apparels
    ]

class ShopApparelQueries:
    # def get_by_id(self, id: int) -> ShopApparelOut:
    #     with pool.connection() as conn:
    #         with conn.cursor() as cur:
    #             cur.execute("""
    #                 SELECT
    #                     *
    #                 FROM shop_apparels
    #                 WHERE id = %s
    #                 """,
    #                 [id]
    #             )
    #             result = cur.fetchone()
    #             return sanitize_shop_apparel([result])[0]

    def get_all(self) -> list[ShopApparelOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM shop_apparels
                    """
                )
                results = cur.fetchall()
                print('results:', results)
                return sanitize_shop_apparel(results)

    def create_shop_apparel(self, shop_apparel: ShopApparelIn) -> ShopApparelOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO shop_apparels (
                        shop_id,
                        apparel_id,
                        quantity
                    ) VALUES (
                        %s, %s, %s
                    )
                    RETURNING *;
                    """,
                    [
                        shop_apparel.shop_id,
                        shop_apparel.apparel_id,
                        shop_apparel.quantity,
                    ]
                )
                result = cur.fetchone()
                return sanitize_shop_apparel([result])[0]

    def update_shop_apparel(self, shop_apparel: ShopApparelIn) -> ShopApparelOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    UPDATE shop_apparels
                    SET
                        quantity = %s
                    WHERE shop_id = %s AND apparel_id = %s
                    RETURNING *;
                    """,
                    [
                        shop_apparel.quantity,
                        shop_apparel.shop_id,
                        shop_apparel.apparel_id,
                    ]
                )
                result = cur.fetchone()
                print("result:", result)
                return sanitize_shop_apparel([result])[0]

    def delete_shop_apparel(self, apparel_id: int, shop_id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    DELETE FROM shop_apparels
                    WHERE apparel_id = %s AND shop_id = %s
                    RETURNING id;
                    """,
                    [apparel_id, shop_id]
                )
                result = cur.fetchone()
                if result is not None:
                    return True
                return False
                
