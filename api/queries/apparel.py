import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class ApparelIn(BaseModel):
    item: str
    description: str
    size: str
    price: int

class ApparelOut(ApparelIn):
    id: int

def sanitize_apparel(apparels):
    return [
        ApparelOut(
            id=apparel[0],
            item=apparel[1],
            description=apparel[2],
            size=apparel[3],
            price=apparel[4],
        )
        for apparel in apparels
    ]

class ApparelQueries:
    def get_by_id(self, id: int) -> ApparelOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM apparels
                    WHERE id = %s
                    """,
                    [id]
                )
                result = cur.fetchone()
                return sanitize_apparel([result])[0]

    def get_all(self) -> list[ApparelOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM apparels
                    """
                )
                results = cur.fetchall()
                print('results:', results)
                return sanitize_apparel(results)

    def create_apparel(self, apparel: ApparelIn) -> ApparelOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO apparels (
                        item,
                        description,
                        size,
                        price
                    ) VALUES (
                        %s, %s, %s, %s
                    )
                    RETURNING *;
                    """,
                    [
                        apparel.item,
                        apparel.description,
                        apparel.size,
                        apparel.price,
                    ]
                )
                result = cur.fetchone()
                return sanitize_apparel([result])[0]

    def update_apparel(self, id: int, apparel: ApparelIn) -> ApparelOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    UPDATE apparels
                    SET
                        item = %s,
                        description = %s,
                        size = %s,
                        price = %s
                    WHERE id = %s
                    RETURNING *;
                    """,
                    [
                        apparel.item,
                        apparel.description,
                        apparel.size,
                        apparel.price,
                        id,
                    ]
                )
                result = cur.fetchone()
                return sanitize_apparel([result])[0]

    def delete_apparel(self, id: int) -> None:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    DELETE FROM apparels
                    WHERE id = %s;
                    """,
                    [id]
                )
